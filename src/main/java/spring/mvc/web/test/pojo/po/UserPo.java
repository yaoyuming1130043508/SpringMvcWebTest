package spring.mvc.web.test.pojo.po;

import lombok.Data;

import java.io.Serializable;
@Data
public class UserPo implements Serializable {
    private Integer id;
    private  String username;
    private  String password;
    private  String  icon;
}
