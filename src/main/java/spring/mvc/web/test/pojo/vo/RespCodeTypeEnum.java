package spring.mvc.web.test.pojo.vo;


public enum RespCodeTypeEnum {
    SUCCESS(0), FAILED(-1);

    private int value;

    RespCodeTypeEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
