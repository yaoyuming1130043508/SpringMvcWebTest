package spring.mvc.web.test.pojo.vo;

/**
 * 返回数据内容标准实体
 *
 * @param <T>
 */
public class RespResult<T> {
    //    业务处理状态 成功:0，失败:非0
    private int code = RespCodeTypeEnum.SUCCESS.getValue();
    //    提示信息
    private String message;
    //    业务数据
    private T data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
