package spring.mvc.web.test.pojo.vo;

public class RespCodeType {
    public final static int SUCCESS = 0;
    public final static int FAILED = -1;
}
