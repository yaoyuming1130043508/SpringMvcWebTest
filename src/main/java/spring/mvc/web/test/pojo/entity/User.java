package spring.mvc.web.test.pojo.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import lombok.Data;

@ApiModel(value = "spring-mvc-base-pojo-entity-User")
@Data
public class User implements Serializable {
    /**
     * 用
     */
    @ApiModelProperty(value = "用户id")
    private Integer id;

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    private String username;

    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String password;

    /**
     * 图片
     */
    @ApiModelProperty(value = "图片")
    private String icon;

    private static final long serialVersionUID = 1L;
}