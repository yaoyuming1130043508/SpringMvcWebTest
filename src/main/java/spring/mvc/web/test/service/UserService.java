package spring.mvc.web.test.service;


import spring.mvc.web.test.pojo.entity.User;
import spring.mvc.web.test.pojo.po.UserPo;
import spring.mvc.web.test.pojo.vo.RespResult;

public interface UserService {
   //用户注册
    RespResult register(UserPo userPo) throws  RuntimeException;
    //用户登录
    RespResult<UserPo> login(UserPo userPo) throws  RuntimeException;
}
