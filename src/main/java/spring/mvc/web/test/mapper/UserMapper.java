package spring.mvc.web.test.mapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Param;
import spring.mvc.web.test.pojo.entity.User;
@Api(tags = "用户的接口")
public interface UserMapper {
@ApiOperation(value = "注册用户的接口")
    int insert(@Param("user") User user);

@ApiOperation(value = "登录用户的接口")
    User selectByUserName(@Param("username") String username,@Param("password")String password);

}