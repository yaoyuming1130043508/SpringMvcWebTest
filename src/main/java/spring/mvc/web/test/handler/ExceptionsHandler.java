package spring.mvc.web.test.handler;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import spring.mvc.web.test.pojo.vo.RespCodeTypeEnum;
import spring.mvc.web.test.pojo.vo.RespResult;

/**
 * 全局异常处理
 */
@ControllerAdvice
@RestControllerAdvice
public class ExceptionsHandler {

    @ResponseBody
    @ExceptionHandler(RuntimeException.class)
    public RespResult handlerException(RuntimeException e) {
        RespResult result = new RespResult();
        result.setCode(RespCodeTypeEnum.FAILED.getValue());
        result.setMessage(e.getMessage());
        return result;
    }
}
